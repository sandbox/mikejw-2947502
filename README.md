# Find Out More module

## What is this?

The **Find Out More** module will put a _Find out more_ link at the bottom of the body field on articles that will link through to Google search using the tags that are on the current article.

## Current limitations

NB: These could potentially be the basis for improvement.

* "Find out more" text in link is hard coded.
* No current way to use a different search engine.
* No automated tests.
* Only works for articles.
* Currently only shows at the bottom of the body field. Would be nice to allow it to be put elsewhere too.
* Not sure if it works with all tags?
* No configuration to turn it on or off for certain articles
